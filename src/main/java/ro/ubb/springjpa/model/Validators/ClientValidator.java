package ro.ubb.springjpa.model.Validators;

import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Entities.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
@Component
public class ClientValidator  {
    public static void validate(Client entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no client. Sorry.\n");
        if (entity.getName().equals("")) throw new ValidatorException("There is no name\n");
        if (entity.getId()<=0) throw new ValidatorException("OOPS! Id issue!!\n");
    }
}
