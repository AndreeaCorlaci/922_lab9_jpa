package ro.ubb.springjpa.repository.DBRepository;

import ro.ubb.springjpa.model.Entities.Book;
import ro.ubb.springjpa.repository.SortingRepository;

public interface BookRepository extends SortingRepository<Book,Long> {
}
