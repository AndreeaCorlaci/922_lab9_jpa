package ro.ubb.springjpa.repository.DBRepository;

import ro.ubb.springjpa.model.Entities.Client;
import ro.ubb.springjpa.repository.SortingRepository;

public interface ClientRepository extends SortingRepository<Client,Long> {
}
