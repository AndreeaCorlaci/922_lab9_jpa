package ro.ubb.springjpa.repository.DBRepository;

import ro.ubb.springjpa.model.Entities.Purchase;
import ro.ubb.springjpa.repository.SortingRepository;

public interface PurchaseRepository extends SortingRepository<Purchase,Long> {
}
