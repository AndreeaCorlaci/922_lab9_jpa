package ro.ubb.springjpa.service.ServiceInterfaces;


import ro.ubb.springjpa.model.Entities.Book;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.repository.Sort;

import java.util.List;

public interface BookService {
    void saveBook(Book book) throws ValidatorException;
    void updateBook(Book b) throws ValidatorException;
    void deleteBook(Long id);
    List<Book> getAllBooks();
    Book getBook(Long id);
    List<Book> getSortedBooks(Sort sort);
}
