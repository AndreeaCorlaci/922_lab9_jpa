package ro.ubb.springjpa.service.ServiceInterfaces;

import ro.ubb.springjpa.model.Entities.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.repository.Sort;

import java.util.List;

public interface ClientService {
    void saveClient(Client c) throws ValidatorException;
    void updateClient(Client c) throws ValidatorException;
    void deleteClient(Long id);
    List<Client> getAllClients();
    Client getClient(Long id);
    List<Client> getSortedClients(Sort sort);
}
